<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 8/8/2019
 * Time: 12:14 PM
 */



$data['breadcrumb'] = [
    [
        'name' => 'Home',
        'href' => route('member.dashboard'),
        'icon' => 'fa fa-home',
    ],
    [
        'name' => 'General Settings',
        'href' => route('member.users.index'),
    ],
    [
        'name' => 'Create',
    ],
];

$data['data'] = [
    'name' => 'General Settings',
    'title'=>'General Settings',
    'heading' => 'General Settings',
];

?>
@extends('layouts.back-end.master', $data)

@push('styles')

    <link rel="stylesheet" href="{{ asset('public/adminLTE/bower_components/select2/dist/css/select2.min.css')}}">

    <style type="text/css">
        .select2.select2-container.select2-container--default.select2-container--focus, .select2.select2-container.select2-container--default.select2-container--below, .select2.select2-container.select2-container--default {
            width: 100% !important;
        }
    </style>
@endpush

@section('contents')
    <div class="row">
        <!-- left column -->
        <div class="col-md-12">

        @include('common._alert')


        <!-- general form elements -->
            <div class="box box-primary">

                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#general_setting" data-toggle="tab">General Setting</a></li>

                    </ul>

                    <div class="tab-content">
                        <div class="active tab-pane" id="general_setting">
                            <div class="box-body">
                                <table class="table table-responsive table-striped">
                                    <tr>
                                        <td> Name </td>
                                        <td> Value </td>
                                    </tr>
                                    <tr>
                                        <td> Member Code</td>
                                        <td> {{ $memberInfo->member_code }}</td>
                                    </tr>
                                    <tr>
                                        <td> Expire Date</td>
                                        <td> {{ formatted_date_string($memberInfo->expire_date) }}</td>
                                    </tr>
                                    <tr>
                                        <td> API Access Token</td>
                                        <td>{{ $memberInfo->api_access_key }} </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- /.box -->
            </div>


        </div>
    </div>
@endsection

@push('scripts')

    <script src="{{ asset('public/adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>

    <script type="text/javascript">

        $(function () {
            $('.select2').select2()
        });

    </script>

@endpush
