<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 2/26/2019
 * Time: 11:41 AM
 */

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ isset($data['title'])? $data['title']: ""}} - {{  human_words(config('app.name')) }} | Accounting </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    @include('layouts.back-end.partials.styles')
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper">

@include('layouts.back-end.partials.header')

@include('layouts.back-end.partials.left-sidebar')


<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

    @include('layouts.back-end.partials.breadcrumb')

    <!-- Main content -->
        <section class="content">

            @yield('contents')

        </section>
    </div>

    @include('layouts.back-end.partials.footer')
    @include('layouts.back-end.partials.right-sidebar')
</div>
@include('layouts.back-end.partials.scripts')
</body>
</html>
