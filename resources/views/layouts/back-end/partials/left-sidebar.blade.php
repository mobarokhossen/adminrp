<?php
/**
 * Created by PhpStorm.
 * User: Mobarok Hossen
 * Date: 2/26/2019
 * Time: 11:40 AM
 */
?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('public/adminLTE/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p class="username">{{ Auth::user()->full_name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        {{--<form action="#" method="get" class="sidebar-form">--}}
            {{--<div class="input-group">--}}
                {{--<input type="text" name="q" class="form-control" placeholder="Search...">--}}
                {{--<span class="input-group-btn">--}}
                    {{--<button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>--}}
                    {{--</button>--}}
                {{--</span>--}}
            {{--</div>--}}
        {{--</form>--}}
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header text-uppercase">Profile Setting</li>
            <li>
                <a href="{{ route('change_password') }}">
                    <i class="fa fa-key"></i>
                    <span>Change Password</span> </a>
            </li>
            <li class="header">MAIN NAVIGATION</li>
            <li>
                @php
                    $route = Auth::user()->hasRole(['admin','super-admin','developer']) ? 'admin' : 'member';
                    $route .= '.dashboard';
                @endphp
                <a href="{{ route($route) }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>

            
            <li class="header">Admin Settings</li>

            {{-- @if(Auth::user()->can(['super-admin', 'admin'])) --}}
                <li class="treeview">
                <a href="#">
                    <i class="fa fa-gears"></i> <span>Settings</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('member.settings.general_settings') }}"><i class="fa fa-cog"></i> General Settings</a></li>
               </ul>
            </li>
           

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Users</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('member.users.index') }}"><i class="fa fa-user"></i> Users</a></li>
                    <li><a href="{{ route('member.users.create') }}"><i class="fa fa-user-plus"></i> Add User</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-unlock-alt"></i> <span>Roles</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('admin.roles.index') }}"><i class="fa fa-unlock-alt"></i> Roles</a></li>
                    <li><a href="{{ route('admin.roles.create') }}"><i class="fa fa-plus"></i> Add Role</a></li>
                </ul>
            </li>

               

            {{-- @endif --}}
            @if(Auth::user()->hasRole(['super-admin']))
             
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-expeditedssl"></i> <span>Permissions</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ route('admin.permissions.index') }}"><i class="fa fa-unlock-alt"></i> Permissions</a></li>
                        <li><a href="{{ route('admin.permissions.create') }}"><i class="fa fa-plus"></i> Add Permission</a></li>
                    </ul>
                </li>

                
            @endif
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

