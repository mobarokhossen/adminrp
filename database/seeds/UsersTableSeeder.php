<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'full_name' => 'admin',
            'email' => 'admin@demo.com',
            'password' => Hash::make('admin'),
            'phone' => '01000000001',
            'verify_token' => str_random(10),
        ]);

        // Find Admin Role
        $adminRole = Role::where('name', 'admin')->first();
        // Add Admin Roles
        $user->attachRole($adminRole);

        $user = User::create([
            'full_name' => 'developer',
            'email' => 'developer@demo.com',
            'password' => Hash::make('developer'),
            'verify_token' => str_random(10),
        ]);

        // Find Admin Role
        $adminRole = Role::where('name', 'developer')->first();

        // Add User Roles
        $user->attachRole($adminRole);

        $user = User::create([
            'full_name' => 'superadmin',
            'email' => 'superadmin@demo.com',
            'password' => Hash::make('superadmin'),
            'verify_token' => str_random(10),
        ]);

        // Find Super Admin Role
        $superAdminRole = Role::where('name', 'super-admin')->first();
        // Add Super Admin Roles
        $user->attachRole($superAdminRole);

    }
}
