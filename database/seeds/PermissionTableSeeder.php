<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = array(
            array('id' => '71','name' => 'super-admin','display_name' => 'Super Admin','description' => NULL,'status' => 'active','created_at' => '2019-09-24 16:17:55','updated_at' => '2019-09-24 16:17:55'),
            array('id' => '79','name' => 'reports','display_name' => 'Reports','description' => NULL,'status' => 'active','created_at' => '2020-03-01 12:27:06','updated_at' => '2020-03-01 12:27:06'),
            array('id' => '80','name' => 'admin','display_name' => 'Admin','description' => NULL,'status' => 'active','created_at' => '2020-03-01 12:28:41','updated_at' => '2020-03-01 12:28:41'),
        );

        foreach ($permissions as $key => $value){

            $data = array();
            $data['name'] = $value['name'];
            $data['display_name'] = $value['display_name'];
            $savePermission = \App\Models\Permission::create($data);
            $savePermission->save();
        }
    }
}
