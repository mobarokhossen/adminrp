<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         // All Table Truncate Before DB Seed
        // $this->call(AllTableTruncateSeeder::class);
//
//         // Start Seeding
            // $this->call(CountryTableSeeder::class);
            $this->call(RolesTableSeeder::class);
            $this->call(PermissionTableSeeder::class);
            $this->call(UsersTableSeeder::class);
            $this->call(SettingsTableSeeder::class);
    }
}
