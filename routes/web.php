<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Auth::routes(['verify' => true]);

// Admin Routes
Route::get('admin', function(){
    return redirect()->route('admin.signin');
});

Route::get('/', ['as' => 'admin.signin', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::get('/verify/registration/{id}', ['as' => 'verify.registration', 'uses' => 'Auth\VerificationController@verifyUser']);
Route::post('login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::post('set_password', ['as' => 'password.set', 'uses' => 'Auth\SetPasswordController@set_password']);
Route::get('user/change-password', ['as' => 'change_password', 'uses' => 'Auth\ChangePasswordController@showChangePasswordForm']);
Route::post('user/change-password', ['as' => 'auth.change_password', 'uses' => 'Auth\ChangePasswordController@changePassword']);



Route::group([
    'prefix' => 'admin',
    'namespace' => 'Admin',
    'as' => 'admin.'
], function () {

    Route::group([
        'middleware' => ['auth', 'role:developer|admin|super-admin'],
    ], function () {


        Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
        Route::resource('roles','RoleController');
        Route::resource('permissions','PermissionController');
        Route::resource('users','UserController');

    });
});


Route::group([
    'prefix' => 'member',
    'namespace' => 'Member',
    'as' => 'member.'
], function () {

    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

    Route::group([
        'middleware' => ['auth', 'role:master-member|developer|admin|super-admin'],
    ], function () {

        Route::resource('settings','SettingController');
        Route::get('company-fiscal-year', ['as' => 'settings.company_fiscal_year', 'uses' => 'SettingController@company_fiscal_year']);
        Route::post('set-company-fiscal-year', ['as' => 'settings.set_company_fiscal_year', 'uses' => 'SettingController@set_company_fiscal_year']);
        Route::get('general-settings', ['as' => 'settings.general_settings', 'uses' => 'SettingController@general_settings']);
        Route::post('set-print-page-setup', ['as' => 'settings.set_print_page_setup', 'uses' => 'SettingController@set_print_page_setup']);
        Route::resource('users','UserController');
        Route::get('set-users-company', ['as' => 'users.set_users_company', 'uses' => 'UserController@set_users_company']);
        Route::post('set-users-company', ['as' => 'users.save_users_company', 'uses' => 'UserController@save_users_company']);
        Route::post('set-cash-setup', ['as' => 'settings.set_cash_setup', 'uses' => 'SettingController@set_cash_setup']);

    });

    Route::group([
        'middleware' => ['auth'],
    ], function () {

      
    });

});

