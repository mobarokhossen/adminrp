<?php

namespace App\Providers;

use App\Events\ProfitBalanceGenerate;
use App\Events\ProfitTransferToEquity;
use App\Listeners\SaveTransferProfitBalanceInEquity;
use App\Listeners\StoreProfitBalance;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        ProfitBalanceGenerate::class => [
            StoreProfitBalance::class,
        ],
        ProfitTransferToEquity::class => [
            SaveTransferProfitBalanceInEquity::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
