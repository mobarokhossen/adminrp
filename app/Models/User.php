<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait { restore as private restoreA; }
    use SoftDeletes { restore as private restoreB; }

    public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name', 'email',  'verify_token', 'status',
    ];

    protected $guarded = [ 'phone', 'password' ];

    protected $dates = ['deleted_at'];

    protected static $logAttributes = ['*'];
    protected static $recordEvents = [ 'updated', 'deleted'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    protected $hidden = [
        'password', 'remember_token', 'verify_token'
    ];

    protected $appends = ['user_details', 'uc_full_name'];


    /**
     * Scope a query to only include active models
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    public function getUserDetailsAttribute()
    {
        return $this->full_name." (".$this->email." ".$this->phone.") ";
    }

    public function getUcFullNameAttribute()
    {
        return ucfirst($this->full_name);
    }

    public function scopeSystemUser($query)
    {
        return $query->whereNotIn('full_name', ['superadmin','developer','master-member']);
    }
}
