<?php
/**
 * Created by PhpStorm.
 * User: R-Creation
 * Date: 3/10/2019
 * Time: 2:50 PM
 */

namespace App\Http\Traits;


use App\Models\User;
use App\Observers\UserObserver;

Trait ObserverTrait
{
    /**
     * Get model observers
     */
    public function getObservers()
    {
        // Model Observers
        // User::observe(UserObserver::class);
    }
}
