<?php

namespace App\Http\Controllers\Admin;

use App\Models\Item;
use App\Models\Purchase;
use App\Models\Sale;
use App\Models\Transactions;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index(){
        return view('admin.dashboard.index');
    }

}
